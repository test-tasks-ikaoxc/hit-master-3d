using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PrefabExtensions
{
    public static bool IsPrefabAsset(GameObject obj)
    {
        var isPrefabAsset = false;
#if UNITY_EDITOR
        var stage = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        isPrefabAsset = (stage != null && stage.scene == obj.scene)
                          || (UnityEditor.PrefabUtility.IsPartOfAnyPrefab(obj)
                              && UnityEditor.PrefabUtility.IsPartOfPrefabAsset(obj)
                              && !UnityEditor.PrefabUtility.IsPartOfNonAssetPrefabInstance(obj));
#endif
        return isPrefabAsset;
    }
}
