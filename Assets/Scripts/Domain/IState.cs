using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState
{
    public void Enter();
    public void Execute();
    public void Exit();
    public void ToWaypoint(Waypoint waypoint);
    public void OnClick(Vector3 point);
}
