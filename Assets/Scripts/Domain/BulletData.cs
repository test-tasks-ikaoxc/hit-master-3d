using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct BulletData
{
    [SerializeField] private float _speed;
    public float Speed => _speed;

    [SerializeField] private int _damage;
    public int Damage => _damage;
}
