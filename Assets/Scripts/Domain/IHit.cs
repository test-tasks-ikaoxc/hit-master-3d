using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHit
{
    public int Hit();
}
