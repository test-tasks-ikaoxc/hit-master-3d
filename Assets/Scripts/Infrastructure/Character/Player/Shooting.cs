using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : IState
{
    private readonly Player _player;

    public Shooting(Player player)
    {
        this._player = player;
    }

    public void Enter()
    {

    }

    public void Execute()
    {
        
    }

    public void Exit()
    {
        
    }

    public void ToWaypoint(Waypoint waypoint)
    {
        _player.State.ChangeState(new Run(_player, waypoint));
    }

    public void OnClick(Vector3 point)
    {
        _player.Shootable.Shoot(_player._shotPoint.position, point);
    }
    
}
