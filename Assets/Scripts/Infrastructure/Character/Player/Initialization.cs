using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialization : IState
{
    private readonly Player _player;
    
    public Initialization(Player player)
    {
        this._player = player;
    }

    public void Enter()
    {
        _player.Navigator.StartNavigation();
        
    }

    public void Execute()
    {
        
    }

    public void Exit()
    {
        
    }

    public void ToWaypoint(Waypoint waypoint)
    {
        _player.State.ChangeState(new Run(_player, waypoint));
    }

    public void OnClick(Vector3 point)
    {
        
    }
}
