using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Run : IState
{
    private readonly Player _player;
    private Waypoint _waypoint;

    public Run(Player player, Waypoint waypoint)
    {
        this._player = player;
        this._waypoint = waypoint;
    }
 
    public void Enter()
    {
        ToWaypoint(_waypoint);
    }
 
    public void Execute()
    {
        if (_player.NavMeshAgent.pathPending)
            return;
        
        if (_player.NavMeshAgent.remainingDistance >= .1f)
            return;
            _player.State.ChangeState(new Shooting(_player));
            _waypoint.Achieve();
        
    }
 
    public void Exit()
    {
        
    }

    public void ToWaypoint(Waypoint waypoint)
    {
        _waypoint = waypoint;
        _player.NavMeshAgent.SetDestination(waypoint.StandPosition.position);
    }

    public void OnClick(Vector3 point)
    {
        
    }
}
