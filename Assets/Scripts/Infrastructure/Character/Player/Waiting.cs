using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waiting : IState
{
    private readonly Player _player;
    
    
    public Waiting(Player player)
    {
        this._player = player;
    }
 
    public void Enter()
    {

    }
 
    public void Execute()
    {
        
    }
 
    public void Exit()
    {
        
    }

    public void ToWaypoint(Waypoint waypoint)
    {
        
    }

    public void OnClick(Vector3 point)
    {
        _player.State.ChangeState(new Initialization(_player));
    }
}
