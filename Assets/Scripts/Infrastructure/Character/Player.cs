using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(WaypointNavigator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Shootable))]
public class Player : MonoBehaviour
{
    [SerializeField] private ClickHandler _clickHandler;
    [SerializeField] protected internal Transform _shotPoint;
    
    protected internal WaypointNavigator Navigator;
    protected internal Shootable Shootable;
    protected internal NavMeshAgent NavMeshAgent;
    
    protected internal readonly StateMachine State = new StateMachine();

    private void OnValidate()
    {
        if (PrefabExtensions.IsPrefabAsset(gameObject) == true)
            return;
        
        if (_clickHandler == null)
            throw new NullReferenceException($"{gameObject.name} - Click Handler - not implemented");
        
        if (_shotPoint == null)
            throw new NullReferenceException($"{gameObject.name} - Shot Point - not setted");
    }
    
    private void Awake()
    {
        Navigator = gameObject.GetComponent<WaypointNavigator>();
        Shootable = gameObject.GetComponent<Shootable>();
        NavMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        
        State.ChangeState(new Waiting(this));
        
        Navigator.WaypointChanged += ToWaypoint;
        Navigator.EndReached += End;
        _clickHandler.OnClick += OnPointerClick;
    }
    
    private void Update()
    {
        State.Update();
    }
    
    private void OnDestroy()
    {
        Navigator.WaypointChanged -= ToWaypoint;
        Navigator.EndReached -= End;
        _clickHandler.OnClick -= OnPointerClick;
    }

    private void ToWaypoint(Waypoint waypoint) => State.CurrentState.ToWaypoint(waypoint);

    private void End()
    {
        State.ChangeState(new Idle(this));
    }
    
    private void OnPointerClick(Vector3 point)
    {
        State.CurrentState.OnClick(point);
    }
}