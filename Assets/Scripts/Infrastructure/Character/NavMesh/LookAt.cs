using System;
using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour 
{
    [SerializeField] private Transform _head = null;
    
    public Vector3 LookAtTargetPosition;
    
    void Start ()
    {
        LookAtTargetPosition = _head.position + transform.forward;
    }
}