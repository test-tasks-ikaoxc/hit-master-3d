
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(LookAt))]
public class LocomotionSimpleNavAgent : MonoBehaviour 
{
    private Animator _animator;
    private NavMeshAgent _agent;
    private LookAt _lookAt;
    
    private Vector2 _smoothDeltaPosition = Vector2.zero;
    private Vector2 _velocity = Vector2.zero;

    private void Awake()
    {
        _animator = gameObject.GetComponent<Animator>();
        _agent = gameObject.GetComponent<NavMeshAgent>();
        _lookAt = gameObject.GetComponent<LookAt>();
        
        _agent.updatePosition = false;
    }

    private void Update()
    {
        var cachedTransform = transform;
        var worldDeltaPosition = _agent.nextPosition - cachedTransform.position;

        // Map 'worldDeltaPosition' to local space
        var dx = Vector3.Dot (cachedTransform.right, worldDeltaPosition);
        var dy = Vector3.Dot (cachedTransform.forward, worldDeltaPosition);
        var deltaPosition = new Vector2 (dx, dy);

        // Low-pass filter the deltaMove
        var smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
        _smoothDeltaPosition = Vector2.Lerp (_smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if time advances
        if (Time.deltaTime > 1e-5f)
            _velocity = _smoothDeltaPosition / Time.deltaTime;

        var shouldMove = _velocity.magnitude > 0.5f && _agent.remainingDistance > _agent.radius;
        
        _animator.SetBool("isMove", shouldMove); // TODO: Delete string relationship
        
        _lookAt.LookAtTargetPosition = _agent.steeringTarget + transform.forward;
        
        if (worldDeltaPosition.magnitude > _agent.radius)
            transform.position = _agent.nextPosition - 0.95f * worldDeltaPosition;
    }

    private void OnAnimatorMove()
    {
        // Update position based on animation movement using navigation surface height
        var position = _animator.rootPosition;
        position.y = _agent.nextPosition.y;
        transform.position = position;
        
    }
}
