using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Enemy : MonoBehaviour, IDamageable
{
    private Health _health;

    public Action<Vector3> Hit;
    public Action<Enemy> Dead;
    
    private void Awake()
    {
        _health = gameObject.GetComponent<Health>();
    }

    private void OnEnable()
    {
        _health.Changed += OnHealthChanged;
    }

    private void OnDisable()
    {
        _health.Changed -= OnHealthChanged;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<IHit>(out var hit))
        {
            Hit?.Invoke(other.gameObject.transform.position);
            TakeDamage(hit.Hit());
        };
    }

    public void TakeDamage(int damage)
    {
        if (damage < 0)
            throw new ArgumentException("damage is negative");
        
        _health.Decrease(damage);
    }

    private void OnHealthChanged(int health)
    {
        if (health <= 0)
            Death();
    }
    
    private void Death()
    {
        Dead?.Invoke(this);
    }
}
