using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int _health;
    
    [SerializeField] 
    private int _maxHealth;
    public int MaxHealth => _maxHealth;
    
    public Action<int> Changed;

    public void Decrease(int health)
    {
        if (health < 0)
            throw new NullReferenceException($"{gameObject.name} - Health - Try Decrease negative value");
        
        _health = Math.Max(_health - health, 0);
        
        Changed?.Invoke(_health);
    }
}
