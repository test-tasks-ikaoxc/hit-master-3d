using System;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class HealthPresenter : MonoBehaviour
{
    [SerializeField] private Health _health;
    
    private MaterialPropertyBlock _materialPropertyBlock;
    private MeshRenderer _renderer;
    private Camera _camera;

    private void OnValidate()
    {
        if (_health == null)
            throw new NullReferenceException($"{gameObject.name} - Health - not implemented");
    }

    private void OnEnable()
    {
        _health.Changed += Present;
    }

    private void OnDisable()
    {
        _health.Changed -= Present;
    }

    private void Awake() 
    {
        _renderer = gameObject.GetComponent<MeshRenderer>();
        _materialPropertyBlock = new MaterialPropertyBlock();
    }

    private void Start() 
    {
        _camera = Camera.main;
    }
    
    private void Present(int health)
    {
        if (health < _health.MaxHealth && health > 0) 
        {
            _renderer.enabled = true;
            AlignCamera();
            UpdateParams(health);
        } 
        else 
        {
            _renderer.enabled = false;
        }
    }

    private void UpdateParams(int health) 
    {
        _renderer.GetPropertyBlock(_materialPropertyBlock);
        _materialPropertyBlock.SetFloat("_Fill", health / (float) _health.MaxHealth); // TODO: String To Key
        _renderer.SetPropertyBlock(_materialPropertyBlock);
    }

    private void AlignCamera()
    {
        if (_camera == null) 
            return;
        
        var camXform = _camera.transform;
        var forward = transform.position - camXform.position;
        forward.Normalize();
        var up = Vector3.Cross(forward, camXform.right);
        transform.rotation = Quaternion.LookRotation(forward, up);
    }
}
