using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(WaypointNavigator))]
public class WaypointEnd : MonoBehaviour
{
   private WaypointNavigator _waypointNavigator;

   private void Awake()
   {
      _waypointNavigator = gameObject.GetComponent<WaypointNavigator>();
   }

   private void OnEnable()
   {
      _waypointNavigator.EndReached += OnEnd;
   }

   private void OnDisable()
   {
      _waypointNavigator.EndReached -= OnEnd;
   }

   private void OnEnd()
   {
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
   }
}
