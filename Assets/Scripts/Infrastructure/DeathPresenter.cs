using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Enemy))]
public class DeathPresenter : MonoBehaviour
{
    [SerializeField] private GameObject _deathEffects;
    [SerializeField] private Collider _collider;
    [SerializeField] private Rigidbody _rigidbody;
    private Enemy _enemy;
    private Animator _animator;

    private void OnValidate()
    {
        if (_deathEffects == null)
            throw new NullReferenceException($"{gameObject.name} - Death effect not implemented");
        
        if (_collider == null)
            throw new NullReferenceException($"{gameObject.name} - Collider not implemented");

        if (_rigidbody == null)
            throw new NullReferenceException($"{gameObject.name} - Rigidbody not implemented");
    }

    private void Awake()
    {
        _enemy = gameObject.GetComponent<Enemy>();
        _animator = gameObject.GetComponent<Animator>();

        SetRagdoll(false);
    }

    private void OnEnable()
    {
        _enemy.Dead += OnDead;
    }

    private void OnDisable()
    {
        _enemy.Dead -= OnDead;
    }

    private void OnDead(Enemy enemy)
    {
        Instantiate(_deathEffects, enemy.transform.position, Quaternion.identity);
        SetRagdoll(true);
    }

    private void SetRagdoll(bool isEnabled)
    {
        _rigidbody.isKinematic = isEnabled;
        _collider.enabled = !isEnabled;
        _animator.enabled = !isEnabled;
        
        var bodies = GetComponentsInChildren<Rigidbody>();
        foreach (var rb in bodies)
        {
            rb.isKinematic = !isEnabled;
        }
    }
}
