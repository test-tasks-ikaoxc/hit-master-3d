using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointNavigator : MonoBehaviour
{
    [SerializeField] private List<Waypoint> _waypoints = new List<Waypoint>();
    private Waypoint _currentWaypoint;
    private int _currentWaypointId;

    public Action<Waypoint> WaypointChanged;
    public Action EndReached;

    public void StartNavigation()
    {
        SetCurrentWaypoint(0);
    }

    private void NextWaypoint()
    {
        var nextWaypointId = _currentWaypointId + 1;

        if (_waypoints.Count - 1 < nextWaypointId)
        {
            EndReached?.Invoke();
            return;
        }

        SetCurrentWaypoint(nextWaypointId);
    }

    private void SetCurrentWaypoint(int id)
    {
        if (_currentWaypoint != null)
            Unsubscribe(_currentWaypoint);
        
        _currentWaypointId = id;
        _currentWaypoint = _waypoints[id];
        
        Subscribe(_currentWaypoint);
        
        WaypointChanged?.Invoke(_currentWaypoint);
    }

    private void Unsubscribe(Waypoint waypoint)
    {
        waypoint.Completed -= NextWaypoint;
    }

    private void Subscribe(Waypoint waypoint)
    {
        waypoint.Completed += NextWaypoint;
    }
    
}
