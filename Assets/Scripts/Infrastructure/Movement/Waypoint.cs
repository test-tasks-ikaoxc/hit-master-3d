using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyRadar))]
public class Waypoint : MonoBehaviour
{
    [SerializeField] private EnemyRadar _enemyRadar;
    
    [SerializeField] 
    private Transform _standPosition;
    public Transform StandPosition => _standPosition;
    
    public Action Completed;
    
    private void OnValidate()
    {
        if (_standPosition == null)
            throw new NullReferenceException($"{gameObject.name} - Stand Position - not setted");
    }

    private void Awake()
    {
        _enemyRadar = gameObject.GetComponent<EnemyRadar>();
    }

    private void OnEnable()
    {
        _enemyRadar.Changed += OnEnemiesChanged;
    }
    
    private void OnDisable()
    {
        _enemyRadar.Changed -= OnEnemiesChanged;
    }

    public void Achieve()
    {
        OnEnemiesChanged();
    }
    
    private void OnEnemiesChanged()
    {
        if (_enemyRadar.HasEnemies == true) 
            return;
        
        Completed?.Invoke();
    }
}
