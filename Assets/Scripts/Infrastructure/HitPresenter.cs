using System;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class HitPresenter : MonoBehaviour
{
    [SerializeField] private GameObject _hitEffect;
    private Enemy _enemy;

    private void OnValidate()
    {
        if (_hitEffect == null)
            throw new NullReferenceException($"{gameObject.name} - Effects - hit effect not setted");
    }

    private void Awake()
    {
        _enemy = gameObject.GetComponent<Enemy>();
    }

    private void OnEnable()
    {
        _enemy.Hit += OnHit;
    }

    private void OnDisable()
    {
        _enemy.Hit -= OnHit;
    }

    private void OnHit(Vector3 position)
    {
        Instantiate(_hitEffect, position, Quaternion.identity);
    }
}
