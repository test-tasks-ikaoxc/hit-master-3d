using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickHandler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private float _rayPointDistance = 15;
    [SerializeField] private LayerMask _environmentLayer;
    private Camera _camera;
    
    public event Action<Vector3> OnClick;

    private void OnValidate()
    {
        if (_rayPointDistance <= 0)
            throw new NullReferenceException($"{gameObject.name} - Click Ray Distance - Less than 0");
    }

    private void Awake()
    {
        _camera = Camera.main;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var ray = _camera.ScreenPointToRay(eventData.position);

        var point = ray.GetPoint(_rayPointDistance);

        if (Physics.Raycast(ray, out var info, _rayPointDistance, _environmentLayer))
        {
            point = info.point;
        }

        OnClick?.Invoke(point);
    }
}
