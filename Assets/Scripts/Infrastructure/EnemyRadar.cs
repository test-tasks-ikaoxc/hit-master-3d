using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyRadar : MonoBehaviour
{
    private readonly List<Enemy> _enemies = new List<Enemy>();
    
    public bool HasEnemies => _enemies.Any();
    
    public Action Changed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Enemy>(out var enemy) == false)
            return;

        AddEnemy(enemy);
    }
    

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent<Enemy>(out var enemy) == false)
            return;

        RemoveEnemy(enemy);
    }
    
    private void AddEnemy(Enemy enemy)
    {
        if (_enemies.Contains(enemy) == true)
            throw new Exception("Already exist");

        enemy.Dead += OnPlayerDead;
        
        _enemies.Add(enemy);

        Changed?.Invoke();
    }
    
    private void RemoveEnemy(Enemy enemy)
    {
        if (_enemies.Contains(enemy) == false)
            throw new Exception("Not exists?");

        enemy.Dead -= OnPlayerDead;
        
        _enemies.Remove(enemy);

        Changed?.Invoke();
    }

    private void OnPlayerDead(Enemy enemy)
    {
        RemoveEnemy(enemy);
    }
}
