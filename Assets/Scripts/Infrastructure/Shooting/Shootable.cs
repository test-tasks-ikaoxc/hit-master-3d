using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shootable : MonoBehaviour
{
    [SerializeField] private BulletPool _bulletPool;
    [SerializeField] private BulletData _bulletData;

    private Bullet _bullet => _bulletPool.AvailableBullet;

    private void OnValidate()
    {
        if (PrefabExtensions.IsPrefabAsset(gameObject) == true)
            return;
        
        if (_bulletPool == null)
            throw new NullReferenceException($"{gameObject.name} - Bullet Pool - not implemented");
    }

    public void Shoot(Vector3 from, Vector3 to)
    {
        _bullet.Fly(from, to, _bulletData);
    }

}