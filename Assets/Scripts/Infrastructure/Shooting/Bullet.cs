using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IHit
{
    [SerializeField] private float _liveTime = 10;
    private int _damage;

    private void OnCollisionEnter(Collision collision)
    {
        Disable();
    }

    private void OnTriggerEnter(Collider other)
    {
        Disable();
    }
    
    public void Fly(Vector3 from, Vector3 to, BulletData bulletData)
    {
        _damage = bulletData.Damage;
        transform.position = from;
        
        var direction = (to - from).normalized;

        StartCoroutine(FlyUpdate(direction, bulletData.Speed));
    }
    
    private IEnumerator FlyUpdate(Vector3 direction,float speed)
    {
        var time = 0f;

        while (time < _liveTime)
        {
            transform.Translate(direction * (speed * Time.deltaTime));
            yield return null;
            time += Time.deltaTime;
        }
        
        Disable();
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    public int Hit()
    {
        return _damage;
    }
}
